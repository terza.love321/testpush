import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import NavbarAdmin from './Component/Navbar/NavbarAdmin'

class AppAdmin extends Component {
    constructor(props){
        super(props)
        this.state = {
            
        }
    }
    render() {
        return (
            <React.Fragment>
                <header className="admin-navbar">
                    <NavbarAdmin />
                    </header>
                <div className = "set-page">
                    {this.props.children}
                </div>
                <footer className="admin-footer">footer</footer>
            </React.Fragment>
        )
    }
}

export default withRouter(AppAdmin)