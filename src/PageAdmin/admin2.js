import React, { Component } from 'react'
import { news } from '../Component/MobData/Data'

export default class admin2 extends Component {

    constructor(props) {
        super(props)
        this.state = {
            newsId: this.props.location.state.id,
            kk: {}
        }
    }

    componentDidMount() {
        this.fetchItem()
    }

    async fetchItem() {
        try {
            const items = await news.filter(ele => ele.id === this.state.newsId)
            this.setState({ kk: items[0] })
        } catch (error) {
            console.log('error', error)
        }
    }

    render() {
        console.log(this.props.location.state.id)
        let { kk } = this.state
        return (
            <div className="mt-3">
                <a className="text-info " style={{ cursor: 'pointer' }}
                    onClick={() => { this.props.history.goBack() }} >กลับ</a>
                <h3>{kk.title}</h3>
                <img src={kk.img} alt="" />
                <p>{kk.detail}</p>
            </div >
        )
    }
}
