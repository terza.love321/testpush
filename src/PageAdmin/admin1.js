import React, { Component } from 'react'
import { news } from '../Component/MobData/Data'
import { InputFormGroup } from '../Component/Form/Input'
import  Images  from '../Component/Images/Images'


export default class admin1 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nn: []
        }
    }

    componentDidMount() {
        this.fetchItem()
    }

    async fetchItem() {
        try {
            const items = await news
            this.setState({ nn: items, oldnn: items })
        } catch (error) {
            console.log('error', error)
        }
    }

    onChangeText = (element) => {
        this.setState({ [element.target.name]: element.target.value })
    }

    onSrarch = (element) => {
        let { oldnn } = this.state
        let value = element.target.value

        if (value !== '') {
            let solution = oldnn.filter(ele => {
                return (
                    ele.title.includes(value)
                )
            })
            this.setState({ nn: solution })
        } else {
            this.setState({ nn: oldnn })
        }
    }

    moreDetail = (id) => {
        this.props.history.push({
            pathname: '/admin2/',
            state: { id }
        })
    }

    render() {
        let { nn } = this.state
        return (
            <div>
                <div className="mt-5">
                    <InputFormGroup data={{ title: 'ค้นหา', md: 6 }} onChangeText={this.onSrarch} />
                    <img src={Images.Asset1} alt="" />
                </div>
                <div className="mt-5">
                    {nn.map((element, i) =>
                        <div className="d-flex align-items-center">
                            <h5>{i + 1}.{element.title}</h5>
                            <span className="text-info pb-1" style={{ cursor: 'pointer' }}
                                onClick={() => this.moreDetail(element.id)}>&nbsp; อ่านต่อ</span>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}
