import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import NavbarUser from './Component/Navbar/NavbarUser'

class AppUser extends Component {
    constructor(props){
        super(props)
        this.state = {
            
        }
    }
    render() {
        return (
            <React.Fragment>
                <header className="user-navbar">
                    <NavbarUser />
                    </header>
                <div className = "set-page">
                    {this.props.children}
                </div>
                <footer className="user-footer">footer</footer>
            </React.Fragment>
        )
    }
}

export default withRouter(AppUser)