import React, { Component } from 'react'
import { Profile, Profile1, Button } from '../Component/Profile/Profile'


export default class Page1 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: 'patikarn',
            city: 'udon',
            show: false
        }
    }
    onShow = () => {
        this.setState({ show: !this.state.show })
    }
    //-------------------------------------------------------
    gotoPage2p = (row) => {
        this.props.history.push({
            pathname: '/page2/' + this.state.city
        });
    };
    //-------------------------------------------------------
    // gotoPage2 = (row) => {
    //     let { name } = this.state
    //     this.props.history.push({
    //         pathname: '/page2',
    //         state: { name }
    //     });
    // };
    render() {
        let { name, city } = this.state
        return (
            <div>
                This is Page1
                <Profile s1={name} s2={city} ss={this.gotoPage2p} />
                <button onClick={this.gotoPage2}>gotoPage2p</button>
                {this.state.show && <div>
                    <Profile1 s1={name} s2={city} />
                    {/* <Button>Show/Hide</Button> */}
                </div>}
            </div>
        )
    }
}