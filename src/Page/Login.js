import React, { Component } from 'react'
import { Row, Col } from 'reactstrap'

const user = {
    name: 'aaa',
    password: '1234'
}
const admin = {
    name: 'admin',
    password: 'admin'
}


export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
    }
    onChangeText = (e) => {
        this.setState({ [e.target.name]: e.target.value })
        console.log('username', this.state.username)
        console.log('password', this.state.password)
    }
    CheckLogin = () => {
        let { username, password } = this.state
        if (username === user.name && password === user.password) {
            window.localStorage.setItem('role', 'user')
            window.location.reload()
        } else if (username === admin.name && password === admin.password) {
            window.localStorage.setItem('role', 'admin')
            window.location.reload()
        } else {
            alert('user name or password in correct')
        }
    }
    render() {
        return (
            <div>
                <Row>
                    <Col xs={12} lh={5}>
                        <div className="form-group">
                            <label>user name</label>
                            <input className="form-control" name="username" onChange={this.onChangeText} />
                        </div>
                        <div className="form-group">
                            <label>password</label>
                            <input className="form-control" name="password" onChange={this.onChangeText} />
                        </div>
                    </Col>
                </Row>
                <button onClick={this.CheckLogin}>Login</button>
            </div>
        )
    }
}
