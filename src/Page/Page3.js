import React, { Component } from 'react'
import { someting } from '../Component/MobData/Data'


export default class Page3 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ss: []
        }
    }

    componentDidMount() {
        this.fetchItem()
    }

    async fetchItem() {
        try {
            const items = await someting
            this.setState({ ss: items })
            console.log('item', items)
        } catch (error) {
            console.log('error', error)
        }
    }

    render() {
        let { ss } = this.state
        return (
            <div>
                {ss.map((element) =>
                    <div>
                        name : {element.name}<br />
                        city : {element.city}<br />
                        skill : {element.skills.map((element2)=>
                        <div>
                            {element2.language},
                        </div>
                        )}
                    </div>
                )}
            </div>
        )
    }
}
