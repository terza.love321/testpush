import React, { Component } from 'react'
import Images from '../Component/Images/Images'
import { div } from '../Asset/scss/MainStyle.scss'

export default class PageTest extends Component {
    render() {
        return (
            <div>
                <div className="div">
                    <i className="fas fa-times-circle" ></i>
                    <div className="divImg">
                        <img className = "img" src={Images.AssetHeader} alt="" />
                    </div>
                    <div className="divPara">
                        <span>4 days ago</span>
                        <strong>Post One</strong>
                        <p>asdjfj'pasdfojgkfdmlakdjfapweo
                            adsfh;kldfgm'sdflkgjs'kldfgms
                            jka;dsf;jhgflkjdf
                        </p>
                    </div>
                    <div className="divFoot">
                        <div >
                            <strong>4 th</strong>
                            <span>asdf</span>
                        </div>
                        <div>
                            <strong>5123</strong>
                            <span>asdpofj</span>
                        </div>
                        <div>
                            <strong>32</strong>
                            <span>adsofj</span>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
