import React, { lazy } from 'react'
import { Switch, Route } from 'react-router-dom'

const AppAdmin = lazy(() => import('../AppAdmin'))

const admin1 = lazy(() => import('../PageAdmin/admin1'))
const admin2 = lazy(() => import('../PageAdmin/admin2'))
const pageTest = lazy(() => import('../Page/PageTest'))


export default () =>
    <AppAdmin>
        <Switch>
            <Route path="/" exact component={admin1} />
            <Route path="/admin1" exact component={admin1} />
            <Route path="/admin2" exact component={admin2} />
            <Route path="/pageTest" exact component={pageTest} />
        </Switch>
    </AppAdmin>