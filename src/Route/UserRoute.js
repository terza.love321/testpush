import React, { lazy } from 'react'
import { Switch, Route } from 'react-router-dom'

const AppUser = lazy(() => import('../AppUser'))

const Page1 = lazy(() => import('../Page/Page1'))
const Page2 = lazy(() => import('../Page/Page2'))
const Page3 = lazy(() => import('../Page/Page3'))

export default () =>
    <AppUser>
        <Switch>
            <Route path="/" exact component={Page1} />
            <Route path="/page1" exact component={Page1} />
            <Route path="/page2" exact component={Page2} />
            <Route path="/page3" exact component={Page3} />
        </Switch>
    </AppUser>