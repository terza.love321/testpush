import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import MainRoute from './Route/MainRoute'
import * as serviceWorker from './serviceWorker';
import './Asset/scss/MainStyle.scss'


ReactDOM.render(<MainRoute />, document.getElementById('root'));

serviceWorker.unregister();
