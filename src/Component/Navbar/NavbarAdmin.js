import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class NavbarAdmin extends Component {
    logout = () =>{
        window.localStorage.removeItem('role')
        window.location.reload()
    }
    render() {
        let path = window.location.oathname
        return (
            <div className="set-admin-navbar">
                navbar
                <Link className={path === '/admin1' ? 'active' : ''} to={'/admin1'}>admin1</Link>
                <Link className={path === '/admin2' ? 'active' : ''} to={'/admin2'}>admin2</Link>
                <Link className={path === '/pageTest' ? 'active' : ''} to={'/pageTest'}>page test</Link>
                <button onClick={this.logout}>log out</button>
            </div>
        )
    }
}
