import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class NavbarUser extends Component {
    logout = () =>{
        window.localStorage.removeItem('role')
        window.location.reload()
    }
    render() {
        let path = window.location.oathname
        return (
            <div className="set-user-navbar">
                navbar
                <Link className={path === '/page1' ? 'active' : ''} to={'/page1'}>page1</Link>
                <Link className={path === '/page2' ? 'active' : ''} to={'/page2'}>page2</Link>
                <Link className={path === '/page3' ? 'active' : ''} to={'/page3'}>page3</Link>
                <button onClick={this.logout}>log out</button>
            </div>
        )
    }
}
